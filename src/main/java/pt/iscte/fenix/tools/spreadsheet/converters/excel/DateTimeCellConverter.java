package pt.iscte.fenix.tools.spreadsheet.converters.excel;

import org.joda.time.DateTime;

import pt.iscte.fenix.tools.spreadsheet.converters.CellConverter;

public class DateTimeCellConverter implements CellConverter {
    @Override
    public Object convert(Object source) {
        return (source != null) ? ((DateTime) source).toDate() : null;
    }
}
