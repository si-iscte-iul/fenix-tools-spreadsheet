package pt.iscte.fenix.tools.spreadsheet.styles;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class FontWeight extends SpreadsheetCellStyle {

    private final short boldweight;

    public FontWeight(short boldweight) {
        this.boldweight = boldweight;
    }

    @Override
    protected void appendToStyle(Workbook book, CellStyle style, Font font) {
        font.setBoldweight(boldweight);
    }

    @Override
    public CellStyle getStyle(Workbook book) {
        CellStyle style = book.createCellStyle();
        Font font = book.createFont();
        appendToStyle(book, style, font);
        style.setFont(font);
        return style;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof FontWeight) {
            FontWeight fontWeight = (FontWeight) obj;
            return boldweight == fontWeight.boldweight;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return boldweight;
    }
}
