package pt.iscte.fenix.tools.spreadsheet.styles;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Workbook;

public class CellAlignment extends SpreadsheetCellStyle {

    private final short align;

    public CellAlignment(short align) {
        this.align = align;
    }

    @Override
    protected void appendToStyle(Workbook book, CellStyle style, Font font) {
        style.setAlignment(align);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof CellAlignment) {
            CellAlignment cellAlignment = (CellAlignment) obj;
            return cellAlignment.align == align;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return align;
    }
}
