package pt.iscte.fenix.tools.spreadsheet.converters.matcher;

import java.util.Map;

import pt.iscte.fenix.tools.spreadsheet.converters.CellConverter;

public interface ConverterMatcher {

    Object convert(Map<Class<?>, CellConverter> converters, Object content);

}
