package pt.iscte.fenix.tools.spreadsheet;

public class Cell {
    private Object value;
    private short span;

    public Cell(Object value, short span) {
        this.value = value;
        this.span = span;
    }

    public Object getValue() {
        return value;
    }

    public short getSpan() {
        return span;
    }


}