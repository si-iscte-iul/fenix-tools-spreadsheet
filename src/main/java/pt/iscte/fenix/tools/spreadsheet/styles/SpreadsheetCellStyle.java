package pt.iscte.fenix.tools.spreadsheet.styles;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Workbook;

import pt.iscte.fenix.tools.spreadsheet.styles.ICellStyle;

public abstract class SpreadsheetCellStyle implements ICellStyle {
    public CellStyle getStyle(Workbook book) {
        CellStyle style = book.createCellStyle();
        appendToStyle(book, style, null);
        return style;
    }

    protected abstract void appendToStyle(Workbook book, CellStyle style, Font font);
}
